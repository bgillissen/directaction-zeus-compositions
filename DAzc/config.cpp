class CfgPatches {
	class DAzc {
		units[] = {};
		weapons[] = {};
		requiredVersion = 1;
		requiredAddons[] = {};
		version = "0.1";
		versionStr = "0.1";
		versionDesc = "Direct Action: Zeus Compositions";
		versionAr[] = {"0.1"};
		author[] = {"Ben"};
	};
};

class CfgMods {
	class DAzc {
		dir="DAzc";
		name="Direct Action: Zeus Compositions";
		picture="";
	};
};

class DAcompo {
	class ZC_TANOA_comp0 {
		author = "Ben";
		name = "Oil Rig";
		picture = "DAzc\preview\oilrig.paa";
		#include "OilRig.Tanoa\mission.sqm"
	};
	class ZC_ALTIS_comp0 {
		author = "Ben";
		name = "Oil Rig";
		picture = "DAzc\preview\oilrig.paa";
		#include "OilRig.Altis\mission.sqm"
	};
	class ZC_ALTIS_comp1 {
		author = "Bob";
		name = "Bob Santa";
		picture = "DAzc\preview\bobSanta.paa";
		#include "BobSanta.Altis\mission.sqm"
	};
};
